package com.acanevali.controller;

import com.acanevali.model.ForestSimulation;
import com.sun.net.httpserver.HttpExchange;

import java.io.IOException;
import java.io.OutputStream;

public class NextStepSimulationController extends GeneralController {

    @Override
    public void handle(HttpExchange exchange) throws IOException {
        try {
            super.handle(exchange);
            this.handleResponse(exchange);
        } catch (IOException ioe) {
            System.err.println(ioe.getMessage());
        }
    }

    private void handleResponse(HttpExchange httpExchange)  throws  IOException {
        ForestSimulation simulation = ForestSimulation.getForestSimulationInstance();
        String forestJSON;

        if(simulation != null){
            if(!simulation.isSimulationOver()){
                simulation.nextStepSimulation();
                simulation.getSimulatedForest().printForest();
            } else {
                simulation.printSimulationStats();
            }
            forestJSON = simulation.getSimulationAsJSON();
        } else {
            forestJSON =  "{ \"error\" : \"No simulation Initiated\" }";
        }

        OutputStream outputStream = httpExchange.getResponseBody();
        httpExchange.getResponseHeaders().add("Content-Type","application/json");
        httpExchange.sendResponseHeaders(200, forestJSON.length());
        outputStream.write(forestJSON.getBytes());
        outputStream.flush();
        outputStream.close();

    }
}
