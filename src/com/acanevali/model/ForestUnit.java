package com.acanevali.model;

public class ForestUnit {
    UnitCoordinate unitCoordinate;
    UnitState unitState;

    public ForestUnit(UnitCoordinate unitCoordinate) {
        this.unitCoordinate = unitCoordinate;
        this.unitState = UnitState.Vivant;
    }

    public UnitCoordinate getUnitCoordinate() {
        return unitCoordinate;
    }

    public void setUnitCoordinate(UnitCoordinate unitCoordinate) {
        this.unitCoordinate = unitCoordinate;
    }

    public UnitState getUnitState() {
        return unitState;
    }

    public void setUnitState(UnitState unitState) {
        this.unitState = unitState;
    }
}
