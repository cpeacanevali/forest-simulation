package com.acanevali.model;

import java.util.Objects;

public class UnitCoordinate {
    private int line_index;
    private int column_index;

    public UnitCoordinate(int line_index, int column_index) {
        this.line_index = line_index;
        this.column_index = column_index;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UnitCoordinate that = (UnitCoordinate) o;
        return line_index == that.line_index && column_index == that.column_index;
    }

    @Override
    public int hashCode() {
        return Objects.hash(line_index, column_index);
    }

    public int getLine_index() {
        return line_index;
    }

    public void setLine_index(int line_index) {
        this.line_index = line_index;
    }

    public int getColumn_index() {
        return column_index;
    }

    public void setColumn_index(int column_index) {
        this.column_index = column_index;
    }
}
